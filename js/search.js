function searchID() {
    var input, filter, ul, li, a, i;
    document.getElementById('percorsi').style.display = "none";
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDevice");
    li = div.getElementsByTagName("a");
    for (i = 0; i < li.length; i++) {
        if (li[i].getAttribute('id').toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}