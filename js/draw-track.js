/*
 * function to draw on the map a line that rapresent the route. It takes in input the selected route ID ("DeviceID/N_ROUTE") and the line color.
 * When it draws the line of a route, it's needed to change the value toggle button stored in the array used to load rows on caption table.
 * There is an optional boolean param only to remove the layer from the map, if it exist, without changing the button. This param is useful when the
 * route has been removed from the selection. 
 */
function drawTrack(route,color,removeonly)
{
	var id='track_'+color;
	var flag= $('button[id='+id+']').attr('aria-pressed'); //if 'true'(string) the coresponding toggle button is swithing from active to inactive and vice versa
	if(removeonly==true)
	{
		if(flag=='true')
		{
		map.removeLayer("track_"+color);
		map.removeSource("track_"+color);
		}
		return;
	}
	if(flag == 'false') 
	{
		var restCall;
		//switch restCall according to mode value(single route or splitted route)
		if(mode==true)
		{
			restCall=urlRest+"/staticRoutenf/"+route;
			//it uses the route id to find the button to modify in the table rows array
			let index=checked.indexOf(route);
		    rows[index].Track='<center><button ID="track_'+color+'" type="button" class="btn btn-default active " data-toggle="button" aria-pressed="true" autocomplete="off" onClick="drawTrack(\''+route+'\',\''+color+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+color+';"></span></center></button></center>';
		}else
		{
			restCall=urlRest+"/allStaticRoutenf/"+route;
			singleRow[0].Track='<center><button ID="track_'+color+'" type="button" class="btn btn-default active " data-toggle="button" aria-pressed="true" autocomplete="off" onClick="drawTrack(\''+route+'\',\''+color+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+color+';"></span></center></button></center>';
		}
		$.ajax({
	    	url : restCall,
	    	success : function (data,stato) 
	    	{
	    		map.addLayer({
	    	        "id": "track_"+color,
	    	        "type": "line",
	    	        "source": {
	    	        	"type": "geojson",
	    	        	"maxzoom": 50,
	    	            "data": {
	    	                "type": "Feature",
	    	                "properties": {},
	    	                "geometry": {
	    	                    "type": "LineString",
	    	                    "coordinates": data[0]["data"]
	    	                }
	    	            }
	    	        },
	    	        "layout": {
	    	            "line-join": "round",
	    	            "line-cap": "round"
	    	        },
	    	        "paint": {
	    	        	"line-color": color,
	    	        	"line-width": 3,
	    	        }
	    	    });    	
	    	},
	    	error : function (richiesta,stato,errori) 
	    	{
	        	alert("E' evvenuto un errore. Lo stato della chiamata 6: "+errori.message);
	   		}
	    });
	    
		
	}
	else
	{
		map.removeLayer("track_"+color);
		map.removeSource("track_"+color);
		if(mode==true)
		{
			//it uses the route id to find the button to modify in the table rows array
			let index=checked.indexOf(route);
			rows[index].Track='<center><button ID="track_'+color+'" type="button" class="btn btn-default" data-toggle="button" aria-pressed="false" autocomplete="off" onClick="drawTrack(\''+route+'\',\''+color+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+color+';"></span></center></button></center>';
		}else
		{
			singleRow[0].Track='<center><button ID="track_'+color+'" type="button" class="btn btn-default" data-toggle="button" aria-pressed="false" autocomplete="off" onClick="drawTrack(\''+route+'\',\''+color+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+color+';"></span></center></button></center>';
		}
	}
}

/*
 * function used to hide all track having the colors passed in input
 */
function hideAllTrack(arrayrow)
{
	for (var i=0;i<arrayrow.length;i++)
		{
			var layerid="track_"+arrayrow[i]['Color'];
			if(map.getLayer(layerid))
				map.setLayoutProperty(layerid, 'visibility', 'none');
		}
		
}


/*
 * function used to show all track having the colors passed in input
 */
function showAllTrack(arrayrow)
{
	for (var i=0;i<arrayrow.length;i++)
	{
		var layerid="track_"+arrayrow[i]['Color'];
		if(map.getLayer(layerid))
			map.setLayoutProperty(layerid, 'visibility', 'visible');
	}
}