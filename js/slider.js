function createSlider(element,urlRest)
{	var restCall;
	element.bind("valuesChanging", function(e, estremi){
		document.getElementById('percorsi').style.display = "none";
		if(mode)
			restCall="sliderDevice";
		else
			restCall="singleDevice";
		$.ajax({
	    	url : urlRest+"/"+restCall+"/"+(isoDate(estremi.values.min)).replace(/ /g, '')+"/"+ (isoDate(estremi.values.max)).replace(/ /g, ''),
	    	success : function (data,stato) 
	    	{
	    		div = document.getElementById("myDevice");
	   			li = div.getElementsByTagName("a");
	    		var filtrati=[];
	   			for(t=0; t<data[0]["data"].length; t++){
	    			filtrati.push(data[0]["data"][t]["DeviceID"]);
	    		}
	    		for (i = 0; i < li.length; i++){
	    			if (filtrati.indexOf(li[i].getAttribute('id')) > -1) {
	            		li[i].style.display = "";
	        		} 
	        		else{
	            		li[i].style.display = "none";
	            	}
	        	}
	   		},
	   		error : function (richiesta,stato,errori) 
	    	{
	        	alert("E' evvenuto un errore. Lo stato della chiamata 4: "+errori.message);
	    	}
	 	});
	});
}