class SingleRoute {
    constructor(percorso,stopwatch,map,color) {
    	this.map=map; //object Mapbox Map
        this.stopwatch=stopwatch; //object stopwatch
        this.color=color; //color associed to this route 
        this.current=-1; //index of actual position in the array
        this.next=this.current + 1; //index of next position
        this.idcompare; //id of the periodic function that perfoms the tracking of this route        
        this.percorso=this.preparePercorso(percorso); //list of position
        this.length=this.percorso.length-1; //number of position 
        this.addPoint(); 
    }
    
    /* 
     * This method takes in input the array obtained from the specific rest call and convert its in another array that contains, for each row,
     * only the TimeIng, Position=[Lat,lon], N_Route and an optional bool(true) when the row rapresents the end of route (TimeExit, Possition=[] in this case)
     * The new array includes one of these ending elements for each single route that makes up the total one
     */
    preparePercorso(percorso)
    {
    	var arrayTimePos=new Array();
    	var route=1;
    	for (var i = 0, len = percorso.length; i < len; i++) 
    	{    		
    		if (percorso[i]["Percorso"] != route)
    		{
    			var TimePos=new Array();
    			TimePos.Time=new Date((percorso[i-1]["TimeExit"]).replace(/\s/g, "T"));
    			TimePos.Position=[];
    			TimePos.Route=route;
    			TimePos.EndRoute=true;
    			route=percorso[i]["Percorso"];
    			arrayTimePos.push(TimePos);
    		}
    		var TimePos=new Array();
    		TimePos.Time=new Date((percorso[i]["TimeIng"]).replace(/\s/g, "T"));
    		TimePos.Position=[percorso[i]["Latitudine"], percorso[i]["Longitudine"]];
    		TimePos.Route=percorso[i]["Percorso"];
    		arrayTimePos.push(TimePos);
    			
    	}
    	var TimePos=new Array();
		TimePos.Time=new Date((percorso[percorso.length -1]["TimeExit"]).replace(/\s/g, "T"));
		TimePos.Position=[];
		TimePos.Route=route;
		arrayTimePos.push(TimePos);
    	return arrayTimePos;
    
    }
    
    /*
     * Method to start a periodic function that perform the tracking.
     */
    start()
    {	
    	this.next=this.current + 1;
    	if(this.idcompare==null && this.current<this.length)
    	{
    		this.idcompare=setInterval(this.compare.bind(this),5);
    	}
    }
    
    /*
     * function that compares actual time of the stopwatch and the next position ingress time. When the comparison is true it increments the indexes,
     * shows on the map the actual position and, if needed, it skips to the next route.
     */
    compare()	
    {
    	var time=this.stopwatch.getTime();
    	if (this.timeCompare(time[0],time[1],time[2],time[3],this.percorso[this.next]["Time"].getHours(),this.percorso[this.next]["Time"].getMinutes(),this.percorso[this.next]["Time"].getSeconds(),this.percorso[this.next]["Time"].getMilliseconds()))
    	{
    		this.current+=1;
    		this.next=this.current+1;
    		if(this.percorso[this.current]["EndRoute"]==true)
    		{
    			//It skips to the next splitted route by setting its start time on the stopwatch
    			this.stopwatch.skipTime([this.percorso[this.next]["Time"].getHours(),this.percorso[this.next]["Time"].getMinutes(),this.percorso[this.next]["Time"].getSeconds(),this.percorso[this.next]["Time"].getMilliseconds()]);
    			this.current+=1;
    			this.next=this.current+1;
    			this.show();
    		}else
    		{
    			this.show();
    			if(this.current==this.length)
    			{
    				//end of route
    				this.stop();
    				this.stopTracking();
    			}
    		}
    	}
    }
    
    /*
     * Method to compare two times
     */
    timeCompare(h1,m1,s1,ms1,h2,m2,s2,ms2)
    {
    	if(h1==h2 && m1==m2 && s1==s2 && ms1>=ms2)
    		return true;
    	if (h1==h2 && m1==m2 && s1>s2 ) 
    		return true;
    	if(h1==h2 && m1>m2)
    		return true;
    	if(h1>h2)
    		return true;
       	return false;
    }
    
    /*
     * Method to pause the tracking 
     */
    stop()
    {
    	if(this.idcompare!==null)
    	{
    	  	clearInterval(this.idcompare);
    	  	this.idcompare=null;
    	}
    }
    
    /*
     * Method to reset the object at the initial state
     */
    reset()
    {	 
    	 this.stop();
    	 map.getSource(this.color).setData({"geometry": {"type": "Point", "coordinates": []}, "type": "Feature", "properties": {}});
    	 this.current=-1;
         this.next=this.current + 1;
    }
    
    /*
     * Method to add the object point and its style on the map without coordinates.
     */
    addPoint()
    { 
    	this.map.addSource(this.color,{
						 "type": "geojson",
						 "data": {   
								 		"type": "Feature",
								 		"geometry":{
								 					"type": "Point",
								 					"coordinates": []
								 		 }
						 }	
    	});
    	
    	this.map.addLayer({
			  "id": this.color,
		      "source": this.color,		 
		      "type": "circle",
		      "paint": {
		            		"circle-radius": 10,
		            		"circle-color": this.color,
		            		"circle-stroke-color": "#000000",
		            		"circle-stroke-width": 1
		        }
		}); 
	
    }
    
    /*
     * Method to show the point on its actual corrdinates
     */
    show()
    {
		map.getSource(this.color).setData({"geometry": {"type": "Point", "coordinates": this.percorso[this.current]["Position"]}, "type": "Feature", "properties": {}});
    }
    
    /*
     * Method to stop the tracking when it's complete. It stops the prograss bar, the stopwatch and disable some
     * buttons of the player
     */
    stopTracking()
    {
    	var elem = document.getElementById("myBar");   
 		var width = (elem.style.width).replace("\%", "");
 		stopB(stopBar);
 		elem.style.width = '100%'; 
        elem.innerHTML = '100%';
    	this.stopwatch.stop();
    	this.stopwatch.setTime([this.percorso[this.current]["Time"].getHours(),this.percorso[this.current]["Time"].getMinutes(),this.percorso[this.current]["Time"].getSeconds(),this.percorso[this.current]["Time"].getMilliseconds()]);
    	$('#stopwatch_play').find('span').attr('class', "glyphicon glyphicon-play");
    	$("button[id^='stopwatch_']").prop('disabled',true);
    	$("button[id='stopwatch_stop']").prop('disabled',false);
    }
    
    /*
     * Method to call when the route is removed from a tracking. It's needed to destroy the elements created on the map.
     */
    destroy()
    {
    	map.removeLayer(this.color);
    	map.removeSource(this.color);
    }
}
