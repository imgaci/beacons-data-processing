class Route {
    constructor(arrayRoute,stopwatch,map,color) {
    	this.map=map; //object Mapbox Map
        this.stopwatch=stopwatch; //object stopwatch
        this.color=color;  //color associed to this route 
        this.row=null;	//correspondig row of caption table
        this.last=false;  //bool to set this route like the last one of a multiple tracking
        this.current=-1;  //index of actual position in the array
        this.next=this.current + 1;  //index of next position
        this.idcompare;  //id of the periodic function that perfoms the tracking of this route
        this.length=arrayRoute.length;  //number of position 
        this.arrayRoute=this.prepareArrayRoute(arrayRoute);  //list of position
        this.addPoint(); 
    }
    /* 
     * This method takes in input the array obtained from the specific rest call and convert its in another array with only TimeIng and Position=[Lat,lon]
     * for each row. The new array includes one more element, the last one, that rapresents the end of route (TimeExit, Possition=[])
     */
 
    prepareArrayRoute(arrayRoute)
    {
    	var arrayTimePos=new Object();
    	for (var i = 0, len = this.length; i < len; i++) {
    		arrayTimePos[i]=new Object();
    		arrayTimePos[i]["Time"]=new Date((arrayRoute[i]["TimeIng"]).replace(/\s/g, "T"));
    		arrayTimePos[i]["Position"]=[arrayRoute[i]["Latitudine"], arrayRoute[i]["Longitudine"]];
    		}
    	arrayTimePos[this.length]=new Object();
    	arrayTimePos[this.length]["Time"]=new Date((arrayRoute[this.length - 1]["TimeExit"]).replace(/\s/g, "T"));
    	arrayTimePos[this.length]["Position"]=[];
    	return arrayTimePos;
    
    }
    /*
     * Method to start a periodic function that perform the tracking. It also set a gray color for the corresponding row of the caption table
     * if this route is not started yet.
     */
    start()
    {	
    	this.next=this.current + 1;
    	if(this.idcompare==null && this.current<this.length)
    	{
    		this.idcompare=setInterval(this.compare.bind(this),5);
    		setTimeout(function(){
    					if(this.current == -1)
    					this.changeRowColor(true);
    		}.bind(this),10);
    	}
    }
    
    /*
     * function that compares actual time of the stopwatch and the next position ingress time. When the comparison is true it increments the indexes,
     * shows on the map the actual position and, if needed, it changes the table row color.
     */
    compare()	
    {
    	var time=this.stopwatch.getTime();
    	if (this.timeCompare(time[0],time[1],time[2],time[3],this.arrayRoute[this.next]["Time"].getHours(),this.arrayRoute[this.next]["Time"].getMinutes(),this.arrayRoute[this.next]["Time"].getSeconds(),this.arrayRoute[this.next]["Time"].getMilliseconds()))
    	{
    		this.current+=1;
    		this.next=this.current+1;
    		this.show();
    		if(this.current==this.length)
    		{
    			//end of route
    			this.stop();
    			if(this.last)
    			{
    				this.stopTracking();
    			}
    			this.changeRowColor(true);
    		}
    		else
    			this.changeRowColor(false);
    	}
    }
    
    
    /*
     * Method to compare two times
     */
    timeCompare(h1,m1,s1,ms1,h2,m2,s2,ms2)
    {
    	if(h1==h2 && m1==m2 && s1==s2 && ms1>=ms2)
    		return true;
    	if (h1==h2 && m1==m2 && s1>s2 ) 
    		return true;
    	if(h1==h2 && m1>m2)
    		return true;
    	if(h1>h2)
    		return true;
       	return false;
    }
    
    /*
     * Method to pause the tracking 
     */
    
    stop()
    {
    	if(this.idcompare!==null)
    	{
    	  	clearInterval(this.idcompare);
    	  	this.idcompare=null;
    	}
    }
    
    /*
     * Method to reset the object at the initial state
     */
    reset()
    {	 
    	 this.stop();
    	 map.getSource(this.color).setData({"geometry": {"type": "Point", "coordinates": []}, "type": "Feature", "properties": {}});
    	 this.current=-1;
         this.next=this.current + 1;
         this.changeRowColor(false);
         this.last=false;
    }
 
    /*
     * Method to add the object point and its style on the map without coordinates.
     */
    addPoint()
    { 
    	this.map.addSource(this.color,{
						 "type": "geojson",
						 "data": {   
								 		"type": "Feature",
								 		"geometry":{
								 					"type": "Point",
								 					"coordinates": []
								 		 }
						 }	
    	});
    	
    	this.map.addLayer({
			  "id": this.color,
		      "source": this.color,		 
		      "type": "circle",
		      "paint": {
		            		"circle-radius": 10,
		            		"circle-color": this.color,
		            		"circle-stroke-color": "#000000",
		            		"circle-stroke-width": 1
		        }
		}); 
	
    }
    
    /*
     * Method to show the point on its actual corrdinates
     */
    show()
    {
		map.getSource(this.color).setData({"geometry": {"type": "Point", "coordinates": this.arrayRoute[this.current]["Position"]}, "type": "Feature", "properties": {}});
    }
    
    /*
     * Method to set the row of caption table
     */
    setRow(row)
    {
    	this.row=row;
    }
    
    /*
     * Method to set this route as last
     */
    setLast()
    {
    	this.last=true;
    }
    
    /*
     * Method to set the row table color. It sets gray color with a true input and transparent otherwise.
     */
    changeRowColor(boolean)
    {
    	if(boolean)
    		this.row.css('background-color', '#bfbfbf');
    	else
    		this.row.css('background-color', 'transparent');   	   		
    }
    
    /*
     * Method to stop the tracking when the route is the last one of comparison and it's complete. It stops the prograss bar, the stopwatch and disable some
     * buttons of the player
     */
    stopTracking()
    {
    	var elem = document.getElementById("myBar");   
 		var width = (elem.style.width).replace("\%", "");
 		stopB(stopBar);
 		elem.style.width = '100%'; 
        elem.innerHTML = '100%';
    	this.stopwatch.stop();
    	this.stopwatch.setTime([this.arrayRoute[this.current]["Time"].getHours(),this.arrayRoute[this.current]["Time"].getMinutes(),this.arrayRoute[this.current]["Time"].getSeconds(),this.arrayRoute[this.current]["Time"].getMilliseconds()]);
    	$('#stopwatch_play').find('span').attr('class', "glyphicon glyphicon-play");
    	$("button[id^='stopwatch_']").prop('disabled',true);
    	$("button[id='stopwatch_stop']").prop('disabled',false);
    }
    
    /*
     * Method to call when the route is removed from a tracking. It's needed to destroy the elements created on the map.
     */
    destroy()
    {
    	map.removeLayer(this.color);
    	map.removeSource(this.color);
    }
}
