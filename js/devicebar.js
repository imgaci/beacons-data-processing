/*
 * This function creates a list of devices whit a progressive identifier, start time, end time and total period as sum of the routes single periods.
 */
function devicelist(urlrest)
{
	//rest call to list devices
	$.get( urlRest+'/permanenza/', function( data ) {
	    var devTot=((data[0]["data"])).length;
	    var data=data[0]["data"]
	    var lista = document.getElementById("myDevice"); 
	    for($i=0;$i<devTot;$i++)
			{
			  var a = new Date((data[$i]["Start"]).replace(/\s/g, "T"));
	          var b = new Date((data[$i]["End"]).replace(/\s/g, "T"));
	          var c =  (data[$i]["Period"]).slice(0,2)+"h"+(data[$i]["Period"]).slice(3,5)+"m"+(data[$i]["Period"]).slice(6,8)+"s";
	          lista.innerHTML +='<a href="#" class="list-group-item" style="font-size: 10px;" id="'+data[$i]["DeviceID"]+'">'+"#"+($i+1)+" from "+("0" + a.getHours()).slice(-2)+":"+("0" + a.getMinutes()).slice(-2)+" to " +("0" + b.getHours()).slice(-2) + ":" +("0" + b.getMinutes()).slice(-2)+" period "+c+'</a>';
			}

		//rest calls to get the first time start and last time end among all devices.
		$.get( urlRest+'/permanenza/firstStart', function( minimo ) {
			$.get( urlRest+'/permanenza/lastEnd', function( massimo ) {
				var date1 = new Date((minimo[0]["data"][0]["Primo"]).replace(/\s/g, "T"))
				var minimoOgg=new Date(date1.getTime());
				var date2 = new Date((massimo[0]["data"][0]["Ultimo"]).replace(/\s/g, "T"))
				var massimoOgg=new Date(date2.getTime());
				//it sets the found times as range bounds of the date slider
				$("#slider").dateRangeSlider({
				    bounds: {min: minimoOgg, max: massimoOgg},
				    defaultValues: {min: minimoOgg, max: massimoOgg},
				    formatter: function(value){
				        var hours = value.getHours(),
				        minutes = value.getMinutes();
				        return TwoDigits(hours) + ":" + TwoDigits(minutes);
				    }
				});  
			});
		});
	});
}