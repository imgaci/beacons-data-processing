class Stopwatch {
    constructor(display) {
        this.running = false;
        this.display = display;
        this.iniziale=[0, 0, 0, 0];
        this.times=new Object;
        this.speed=1;
        this.reset();
        this.print();
    }
    
    reset() {
    	// times[hours, miumtes, seconds, milliseconds]
    	this.times[0]=this.iniziale[0];
    	this.times[1]=this.iniziale[1];
    	this.times[2]=this.iniziale[2];
    	this.times[3]=this.iniziale[3];
    	this.speed=1;
    }
    
    getTime(){
    	return this.times;
    }
    
    setTime(time){
    	this.iniziale=time;
    	this.clear();
    }
    
    skipTime(time){
    	this.stop();
    	this.times[0]=time[0];
    	this.times[1]=time[1];
    	this.times[2]=time[2];
    	this.times[3]=time[3];
    	this.print();
    	this.start();
    }
    
    goFaster()
    {
    	if(this.speed<8)
    		this.speed=this.speed*2;
    	return this.speed;
    }
    
    goSlower()
    {
    	if(this.speed>1)
    		this.speed=this.speed/2;
    	return this.speed;
    }
    
    start() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
    }
    
   
    stop() {
        this.running = false;
        this.time = null;
    }

    clear() {
      this.reset();
      this.print()
    }
    
    
    step(timestamp) {
        if (!this.running) return;
        this.calculate(timestamp);
        this.time = timestamp;
        this.print();
        requestAnimationFrame(this.step.bind(this));
        
    }
    
    calculate(timestamp) {
        var diff = (timestamp - this.time)*this.speed;
        // Milliseconds
        this.times[3] += diff ;
        // Seconds are 1000 milliseconds
        /*
        if (this.times[3] >= 10000) {
            this.times[2] += 10;
            this.times[3] -= 10000;
        }else */if (this.times[3] >= 1000) {
            this.times[2] += 1;
            this.times[3] -= 1000;
        }
        // Minutes are 60 seconds
        if (this.times[2] >= 60) {
            this.times[1] += 1;
            this.times[2] -= 60;
        }
        // Hour are 60 minuts
        if (this.times[1] >= 60) {
            this.times[0] += 1;
            this.times[1] -= 60;
        }
        
        
        
    }
    
    print() {
        this.display.innerText = this.format(this.times);
    }
    
    format(times) {
        return `\
${pad0(times[0], 2)}:\
${pad0(times[1], 2)}:\
${pad0(times[2], 2)}:\
${pad0(Math.floor(times[3]), 3)}`;
    }
}

function pad0(value, count) {
    var result = value.toString();
    for (; result.length < count; count)
        result = '0' + result;
    return result;
}


