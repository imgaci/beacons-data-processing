function createPOI(urlRest,map)
{
	$.ajax({
		url : urlRest+"/beacon",
		success : function (data,stato) 
		{
			var beacons = new Array();
			var data = data[0]["data"];		
			for (var i = 0, len = data.length; i < len; i++)
			{
				var beacon = {
					    "type": "Feature",
					    "properties": {
					    "description": "<strong>Beacon "+ data[i]["ID"] +"</strong><p>"+ data[i]["Nome"] +"</p>",
					},
					    "geometry": {
					        "type": "Point",
					        "coordinates": [data[i]["Latitudine"], data[i]["Longitudine"]] 
					    }
					}
				beacons.push(beacon);
			}

			map.loadImage('img/pdi.png', (error, image) => {
		        if (error) throw error;
		        map.addImage('cat', image);
		        map.addLayer({
		            "id": "beacons",
		            "type": "symbol",
		            "source": {
		                "type": "geojson",
		                "data": {
		                    "type": "FeatureCollection",
		                    "features": beacons
		                }
		            },
		            "layout": {
		                "icon-image": "cat",
		                "icon-size": 0.20,
		                "icon-allow-overlap": true 
		            }
		       });
		   });
			// When a click event occurs on a feature in the places layer, open a popup at the
			// location of the feature, with description HTML from its properties.
			map.on('click', 'beacons', function (e) {
			    new mapboxgl.Popup({})
			        .setLngLat(e.features[0].geometry.coordinates)
			        .setHTML(e.features[0].properties.description)
			        .addTo(map);
			});

			// Change the cursor to a pointer when the mouse is over the places layer.
			map.on('mouseenter', 'beacons', function () {
			    map.getCanvas().style.cursor = 'pointer';
			});

			// Change it back to a pointer when it leaves.
			map.on('mouseleave', 'beacons', function () {
			    map.getCanvas().style.cursor = '';
			});
		},
		error : function (richiesta,stato,errori) 
		{
	    	alert("E' evvenuto un errore. Lo stato della chiamata 5: "+errori.message);
		}
	});
}