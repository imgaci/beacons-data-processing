var state = 'stop';
var speed=1;
function buttonBackPress() {
	$("button[id='stopwatch_fw']").prop('disabled',false);
	stopwatch.stop();
	speed=stopwatch.goSlower();
	stopB(stopBar);
	interval=Math.floor((interval)*2);	
	if(state=='play')
	{
		move();
		stopwatch.start();
	}
	if(speed == 1)
		$("button[id='stopwatch_bw']").prop('disabled',true);
	printSpeed();

}

function buttonForwardPress() {
	$("button[id='stopwatch_bw']").prop('disabled',false);
	stopwatch.stop();
	speed=stopwatch.goFaster();
	stopB(stopBar);
	interval=Math.round((interval)/2);
	if(state=='play')
	{
		move();
		stopwatch.start();
	}
	if( speed == 8)
		$("button[id='stopwatch_fw']").prop('disabled',true);
	printSpeed();

}

function buttonPlayPress() {
	var button = $('#stopwatch_play');
    if(state=='stop' || state=='pause')
    {
      if(state=='stop')
      {
    	  $("button[id='stopwatch_stop']").prop('disabled',false);
    	  $("button[id='stopwatch_fw']").prop('disabled',false);
    	  if(mode)
    		  interval=Math.floor(((arrayend[indexmaxend] - arraystart[indexminstart])/100));
    	  else
    		  interval=Math.floor((singlePeriod/100));
      }
      move();
      startTracking();
	  state='play'; 
      button.find('span').attr('class','glyphicon glyphicon-pause');        
    }
    else if(state=='play'){
      pauseTracking();
      stopB(stopBar);
      state = 'pause';
      button.find('span').attr('class', "glyphicon glyphicon-play"); 
    }
	$("button[name='routeremover']").prop('disabled',true);
	printSpeed();
}

function buttonStopPress(){
	if(state=='play' || state=='pause')
	{
		resetBar();
		stopTracking();
    	state = 'stop';
    	speed=1
    	$("button[id^='stopwatch_']").prop('disabled',true);
   		var button = $('#stopwatch_play');
    	button.find('span').attr('class', "glyphicon glyphicon-play");
    	button.prop('disabled',false);
    	$("button[name='routeremover']").prop('disabled',false);
    	if(mode)
    		$('#table').bootstrapTable('load',rows);
    	else
    		$('#table').bootstrapTable('load',singleRow);
    	document.getElementById('speed').style.display = "none";
	}
        
}

function startTracking()
{
	if(mode)
	{
		for(var i=0;i<route.length; i++)
		{
			var row= $('#table').find('tr[data-index = '+i+']');
			route[i].setRow(row);
			route[i].start();
			if(i==indexmaxend)
				route[i].setLast();
		}
	}else{
		singleRoute.start();
	}
	stopwatch.start();	
}

function pauseTracking()
{
	if(mode)
	{
		for(var i=0;i<route.length; i++)
			route[i].stop();
	}else{
		singleRoute.stop();
	}
	stopwatch.stop();
}

function stopTracking()
{
	stopwatch.stop();
	if(mode)
	{
		for(var i=0;i<route.length; i++)
			route[i].reset();
		stopwatch.setTime([arraystart[indexminstart].getHours(),arraystart[indexminstart].getMinutes(),arraystart[indexminstart].getSeconds(),arraystart[indexminstart].getMilliseconds()]);		
	}else{
		singleRoute.reset();
		stopwatch.setTime([singleStart.getHours(),singleStart.getMinutes(),singleStart.getSeconds(),singleStart.getMilliseconds()]);
	}
}

function printSpeed(){
	$('#speed').html('<h4> <b> Speed: '+speed+'x</b></h4>');
	document.getElementById('speed').style.display = "inline-block";

}
