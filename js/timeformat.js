/*
 * This JS contains 3 helpers used to format times and dates as needed
 */


/*
 * Function to prepend 0 to value less then 10 
 */
function TwoDigits(val)
{
		if (val < 10)
	   	{
     		return "0" + val;
		}

	return val;
}

/*
 * function to format a data as YYYY-MM-DD HH:MM:SS.mss
 */
function isoDate(date) {
      return date.getFullYear() +
        '-' + TwoDigits(date.getMonth() +1) +
        '-' + TwoDigits(date.getDate()) +
        ' ' + TwoDigits(date.getHours()) +
        ':' + TwoDigits(date.getMinutes()) +
        ':' + TwoDigits(date.getSeconds()) +
        '.' + (date.getMilliseconds() / 1000).toFixed(3).slice(2, 5);
}

/*
 * this function takes in input a time (HH:MM:SS.mss or HH:MM:SS ) and converts it into milliseconds
 */
function timeString2ms(time) 
{	 
	  var a,b;
	  a=time.split('.');
	  if(a[1] == undefined)
		  b=0;
	  else
		  b=Number(a[1]);	  
	  a=a[0].split(':');
	  return ((Number(a[0])*3600+Number(a[1])*60+Number(a[2]))*1000)+b;
}