
/* Splitted Mode Var*/
var rows=new Array(); //array that contains all rows to load on the caption table
$(function () {$('#table').bootstrapTable({data:rows});});
var route=new Array(); //array that contains all the route objects istance of the Route class 
var arraystart=new Array(); //array that contains all start times of the selected routes 
var arrayend= new Array(); //array that contains all end times of the selected routes
var indexminstart; // index of the minimum between the start times
var indexmaxend; // index of the maximum between the end times
var checked=new Array(); // array that contains the ID ("DeviceID/N_ROUTE") of all checked routes
var color=["#45CD77","#EBEF70","#F26B42"]; //list of colors used for splitted mode
/* Single Mode Var*/
var singleColor = '#c50a0a'; //color used for single mode
var singleRow=new Array(); // it contains the row to load on the caption table
var singleRoute; //array that contains the route object istance of the Route class 
var singleStart; //the start time of the selected route
var singlePeriod; //the route period in milliseconds

/* SPLITTED MODE FUNC*/

/*
 * IMPORTANT: The correct working of these functions is based on the correspondence of the array variables indexes, for each selected route, that are the same for 
 * all variables because all push/delete operations in/from these are done sequentially in the same context.  
 */

/*
 * This function takes in input the event click on device list and it shows all the clicked device routes with checkboxes to select them (maximum of 3 selections). 
 */
function routelist(event) {
    var target = event.target, idClick;
    idClick=target.id;
    $.ajax({
    	//rest call according to clicked device and the time slider window
    	url : urlRest+"/sliderRoute/"+idClick+"/"+(isoDate($("#slider").dateRangeSlider("min"))).replace(/ /g, '')+"/"+ (isoDate($("#slider").dateRangeSlider("max"))).replace(/ /g, ''),
    	success : function (data,stato) 
    	{
    		$('#myPercorsi').html('');
    		for(var i=0; i<((data[0]["data"])).length; i++)
        	{
        		var a = new Date((data[0]["data"][i]["Start"]).replace(/\s/g, "T"));
         		var b = new Date((data[0]["data"][i]["End"]).replace(/\s/g, "T"));
          		var c =  (data[0]["data"][i]["Period"]).slice(0,2)+"h"+(data[0]["data"][i]["Period"]).slice(3,5)+"m"+(data[0]["data"][i]["Period"]).slice(6,8)+"s";
        		var string=idClick+'/'+ data[0]["data"][i]["Route"];
        		//if it has been reached the maximum of 3 selections, all unselected checkboxes are disabled
        		if (checked.length < 3)
            	{
        			//it sets checked property for the already selected routes
        			if(checked.indexOf(string) != -1)
    		    		$('#myPercorsi').append('<label class="list-group-item" style="font-size: 10px;"><input type="checkbox" class="routelist" id="'+string+'" checked/>'+" #"+(i+1)+" from "+("0" + a.getHours()).slice(-2)+":"+("0" + a.getMinutes()).slice(-2)+" to " +("0" + b.getHours()).slice(-2) + ":" +("0" + b.getMinutes()).slice(-2)+" period "+c+ '</label>');
        			else
        				$('#myPercorsi').append('<label class="list-group-item" style="font-size: 10px;"><input type="checkbox" class="routelist" id="'+string+'" />'+" #"+(i+1)+" from "+("0" + a.getHours()).slice(-2)+":"+("0" + a.getMinutes()).slice(-2)+" to " +("0" + b.getHours()).slice(-2) + ":" +("0" + b.getMinutes()).slice(-2)+" period "+c+ '</label>');
            	}
        		else
            	{
        			//it sets checked property for the already selected routes and it disables the other ones
        			if(checked.indexOf(string) != -1)
    		    		$('#myPercorsi').append('<label class="list-group-item" style="font-size: 10px;"><input type="checkbox" class="routelist" id="'+string+'" checked/>'+" #"+(i+1)+" from "+("0" + a.getHours()).slice(-2)+":"+("0" + a.getMinutes()).slice(-2)+" to " +("0" + b.getHours()).slice(-2) + ":" +("0" + b.getMinutes()).slice(-2)+" period "+c+ '</label>');
        			else
        				$('#myPercorsi').append('<label class="list-group-item" style="font-size: 10px;"><input type="checkbox" class="routelist" id="'+string+'" disabled/>'+" #"+(i+1)+" from "+("0" + a.getHours()).slice(-2)+":"+("0" + a.getMinutes()).slice(-2)+" to " +("0" + b.getHours()).slice(-2) + ":" +("0" + b.getMinutes()).slice(-2)+" period "+c+ '</label>');
            	}	
    		
    		}
    		//this is a listener for checkboxes events (check/uncheck) 
    		$('input.routelist[type=checkbox]').change(function(e)
    	 	{
        		var checkedid=$(this)[0]["id"];
        		//case of checked event
    		  	if($(this).prop("checked"))
        	  	{
    				checked.push($(this)[0]["id"]);
    				//if it has been reached the maximum of 3 selections, all unselected checkboxes are disabled
					if (checked.length >= 3)
						$("input:checkbox:not(:checked)").prop('disabled',true);
					//rest call according to checked route
					$.ajax({
				    	url : urlRest+"/percorso/"+checkedid,
				    	success : function (data,stato) 
				    	{
				    		$("button[id='stopwatch_play']").prop('disabled',false);
				    		var circlecolor=color.pop();
				    		//realization and loading of the corespondig table row 
				    		var circle='<svg height="10" width="10"><circle cx="5" cy="5" r="5" fill="'+circlecolor+'" /></svg>';
				    		var remove='<center><button name="routeremover" class="btn btn-default" onClick="removefromCaption(\''+checkedid+'\');"><center><span class="glyphicon glyphicon-trash"  style="color:'+circlecolor+';"></span></center></button></center>';
				    		var track='<center><button ID="track_'+circlecolor+'" type="button" class="btn btn-default" data-toggle="button" aria-pressed="false" autocomplete="off" onClick="drawTrack(\''+checkedid+'\',\''+circlecolor+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+circlecolor+';"></span></center></button></center>';
				    		data[0]["data"][0].Circle=circle;
				    		data[0]["data"][0].Color=circlecolor;
				    		data[0]["data"][0].Trash=remove;
				    		data[0]["data"][0].Track=track;
				    		rows.push(data[0]["data"][0]);   	
				    		$('#table').bootstrapTable('load',rows);
				    		//Calculation of the minimum of start times and calculation of the maximum calulation of end times
				    		arraystart.push(new Date((data[0]["data"][0]["Start"]).replace(/\s/g, "T")));
				    		arrayend.push(new Date((data[0]["data"][0]["End"]).replace(/\s/g, "T")));
				    		indexminstart= arraystart.reduce((iMin, x, i, arr) => x < arr[iMin] ? i : iMin, 0);
				    		indexmaxend = arrayend.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);
				    		//setting time of the stopwatch according to the calulated minimum
				    		stopwatch.setTime([arraystart[indexminstart].getHours(),arraystart[indexminstart].getMinutes(),arraystart[indexminstart].getSeconds(),arraystart[indexminstart].getMilliseconds()]);
				    		//instance and push of the route object
				    		$.ajax({
				    		    url : urlRest+"/posizionenf/"+checkedid,
				    		    success : function (positions,stato) 
				    		    { 
				    			   route.push(new Route(positions[0]["data"],stopwatch,map,circlecolor));    	    
				    	   		},
				    		    error : function (richiesta,stato,errori) {
				    		        alert("E' evvenuto un errore. Lo stato della chiamata 1: "+errori.message);
				    		    }
				    	     });
				    	},
				   		error : function (richiesta,stato,errori) 
				    	{
				        	alert("E' evvenuto un errore. Lo stato della chiamata 2: "+errori.message);
				   		}
					});
				
        	  	}
    		    //case of unchecked event
    		  	else
        	  	{
    			  removefromCaption(checkedid);     		
             	}
    	 	})    
    	},
    	error : function (richiesta,stato,errori) 
    	{
        	alert("E' evvenuto un errore. Lo stato della chiamata 3: "+errori.message);
    	}
	});
    document.getElementById('percorsi').style.display = "block";
}

/*
 * This function takes in input a route ID ("DeviceID/N_ROUTE") and removes its from the selected routes.
 * It is called when there is an unchecked event or when a trash button is pressed
 */
function removefromCaption(checkedid)
{
	$('input:checkbox[id="'+checkedid+'"]:checked').prop('checked',false);
	$('input.routelist[type=checkbox]').prop('disabled',false);
	var index=checked.indexOf(checkedid);
	color.push(rows[index]['Color']);
	drawTrack(checkedid,rows[index]['Color'],true);
	rows.splice(index, 1);
	checked.splice(index, 1);
	arraystart.splice(index, 1);
	arrayend.splice(index, 1);
	route[index].destroy();
	route.splice(index, 1);
	//if the checked list is empty, it sets the stopwatch on null time and disables the player buttons, otherwise it calculates new min and max
	if(checked.length==0)
	{
		stopwatch.setTime([0,0,0,0]);
		indexminstart=null;
		indexmaxend=null;
		$("button[id^='stopwatch_']").prop('disabled',true);
	}
	else
	{	 
		indexminstart= arraystart.reduce((iMin, x, i, arr) => x < arr[iMin] ? i : iMin, 0);
		stopwatch.setTime([arraystart[indexminstart].getHours(),arraystart[indexminstart].getMinutes(),arraystart[indexminstart].getSeconds(),arraystart[indexminstart].getMilliseconds()]);       		
		indexmaxend = arrayend.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);		  		
	}
	$('#table').bootstrapTable('load',rows);
	
}

/* SINGLE MODE FUNC*/

/*
 * This function takes in input the event click on device list and for the clicked one it considers the union of all device routes as the route to track.
 * This mode allow only one track for time, so on change device it removes from tracking the previous one
 */
function singlemode(event)
{
	var target = event.target, idClick;
    idClick=target.id;
    drawTrack(null,singleColor,true);
    singleRow.length=0;
    if(singleRoute!=null)
    	singleRoute.destroy();
    $.ajax({
    	//rest call according to clicked device
    	url : urlRest+"/permanenza/"+idClick,
    	success : function (data,stato) 
    	{
    		$("button[id='stopwatch_play']").prop('disabled',false);
    		//realization and loading of the row
    		var circle='<svg height="10" width="10"><circle cx="5" cy="5" r="5" fill="'+singleColor+'" /></svg>';
    		var remove='<center><button name="routeremover" class="btn btn-default" onClick="removefromsingleCaption();"><center><span class="glyphicon glyphicon-trash"  style="color:'+singleColor+';"></span></center></button></center>';
    		var track='<center><button ID="track_'+singleColor+'" type="button" class="btn btn-default" data-toggle="button" aria-pressed="false" autocomplete="off" onClick="drawTrack(\''+idClick+'\',\''+singleColor+'\')"><center><span class="glyphicon glyphicon-map-marker"  style="color:'+singleColor+';"></span></center></button></center>';
    		data[0]["data"][0].Circle=circle;
    		data[0]["data"][0].Color=singleColor;
    		data[0]["data"][0].Trash=remove;
    		data[0]["data"][0].Track=track;   	
    		singleRow.push(data[0]["data"][0]);
    		$('#table').bootstrapTable('load',singleRow);
    		//conversion of the period into milliseconds and setting of stopwatch start time
    		singlePeriod= timeString2ms(data[0]["data"][0]["Period"]);
    		singleStart=new Date(data[0]["data"][0]["Start"]);
    		stopwatch.setTime([singleStart.getHours(),singleStart.getMinutes(),singleStart.getSeconds(),singleStart.getMilliseconds()]);
    		$.ajax({
    		    url : urlRest+"/posizionenf/"+idClick,
    		    success : function (positions,stato) 
    		    { 
    		    	//instance of the route object
    		    	singleRoute=new SingleRoute(positions[0]["data"],stopwatch,map,singleColor);    	    
    	   		},
    		    error : function (richiesta,stato,errori) {
    		        alert("E' evvenuto un errore. Lo stato della chiamata 8: "+errori.message);
    		    }
    	     });
    	},
   		error : function (richiesta,stato,errori) 
    	{
        	alert("E' evvenuto un errore. Lo stato della chiamata 7: "+errori.message);
   		}
	});
}

/*
 * This function removes the selected device from tracking. It is called when the trash button is pressed
 */
function removefromsingleCaption()
{
	$("button[id^='stopwatch_']").prop('disabled',true);
	drawTrack(null,singleColor,true);
	singleRoute.destroy();
	singleRoute=null;
	singleRow.length=0;
	singleStart=null;
	stopwatch.setTime([0,0,0,0]);
	$('#table').bootstrapTable('load',singleRow);
	
}

/*
 * This function is needed to insert the single routes details in the table after datails button clicking
 */
function detailFormatter(index, row) {
    var html = [];
	var return_routes = function () {
    	var routes = null;
    	//rest call according to clicked device
        $.ajax({
        	'async': false,
	    	url : urlRest+"/percorso/"+row["DeviceID"],
	    	success : function (data,stato){	
	    		routes = data[0]["data"];
	    	},
	   		error : function (richiesta,stato,errori){
	        	alert("E' evvenuto un errore. Lo stato della chiamata: "+errori.message);
	    	}
	    	
	 	});
    	return routes;
	}();
	//html code of requested details 
 	html.push('<h5><b><center>  Routes of Device '+row["DeviceID"]+'</center> </b></h5> <br>');
 	for (var i = 0, len = return_routes.length; i < len; i++){
 		html.push('<p style="font-size:15px"><b> Route:</b> ' + return_routes[i]["Route"] + '<b> Start: </b>' + return_routes[i]["Start"].slice(11,19) +
        '<b> End: </b>' + return_routes[i]["End"].slice(11,19) + '<b> Period: </b>'+ return_routes[i]["Period"].slice(0,8) +'</p>');
   }
   return html.join('');
}
