<!DOCTYPE html>
<html lang="en">
	
	

<head>
	<?php include 'getRest.php';?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Beacons Tracking - Università Politecnica delle Marche</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/iThing.css" rel="stylesheet">
    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/bootstrap-table.css" rel="stylesheet">
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.0/mapbox-gl.css' rel='stylesheet' />
    <link href="css/please-wait.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>




</script>

</head>

<body>
<!-- JavaScript -->
<script type="text/javascript" src="js/please-wait.min.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-table.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.35.0/mapbox-gl.js'></script>
<script src="js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="js/jQRangeSliderMouseTouch.js"></script>
<script type="text/javascript" src="js/jQAllRangeSliders-withRuler-min.js"></script>
<script type="text/javascript" src="js/timeformat.js"></script>
<script type="text/javascript" src="js/stopwatch.js"></script>
<script type="text/javascript" src="js/route.js"></script>
<script type="text/javascript" src="js/singleroute.js"></script>
<script type="text/javascript" src="js/search.js"></script>	
<script type="text/javascript" src="js/routebar.js"></script>
<script type="text/javascript" src="js/devicebar.js"></script>
<script type="text/javascript" src="js/progressbar.js"></script>	
<script type="text/javascript" src="js/player.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<script type="text/javascript" src="js/draw-track.js"></script>
<script type="text/javascript" src="js/poi.js"></script>
<script type="text/javascript">

/* Loading Page according to variable 'loaded' value after 30 seconds*/

var loading_screen = window.pleaseWait({
       logo: "img/logo.png",
       backgroundColor: '#434343',
       loadingHtml: "<p class='loading-message'>Please, Wait.</p> <div class='sk-cube-grid'><div class='sk-cube sk-cube1'></div><div class='sk-cube sk-cube2'></div><div class='sk-cube sk-cube3'></div><div class='sk-cube sk-cube4'></div><div class='sk-cube sk-cube5'></div><div class='sk-cube sk-cube6'></div><div class='sk-cube sk-cube7'></div><div class='sk-cube sk-cube8'></div><div class='sk-cube sk-cube9'></div></div>"
});

var loaded=false;

setTimeout(function(){
	if(!loaded)
	{
		alert('Impossible to load page, please control your internet connection and click OK.');
		location.reload();
	}
},30000);
  	
</script>

<div id="wrapper">
	
 
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <div class="sidebar-brand">
        <h3><center>Beacons Tracking</center> </h3>
   </div>
      	
   <center><input id="track-mode" type="checkbox" checked data-toggle="toggle" data-on="Splitted Route" data-off="Single Route" data-offstyle="warning"></center>
           
   <div style=" border-top: 2px; solid #ddd;" class="panel-heading">
        <h3 style="color: white "><center>Device ID</center> </h3>
   </div>
   
   <input type="text" class="form-control" id="myInput" onkeyup="searchID()" placeholder="Search by Device ID" title="Type in a name">
	 
	 
   <div id="slider" style="margin-top: 5px"></div>
     
     
   <div class="panel-body" >
       <div class="list-group" style="overflow-y: auto; height:160px;" id="myDevice"></div>
            <!-- /.list-group -->
   </div>
     
     
   <div id="percorsi" style="display:none;" class="panel-body">
       <h4 style="color:white"><center>Routes <small>(Max 3 selections)</small></center></h4>
       <div class="list-group" id="myPercorsi"  style="overflow-y: auto; height:170px;"></div>
         <!-- /.list-group -->
   </div>
    
   
</div>
<!-- /#sidebar-wrapper -->
 
<!-- Page Content -->
<div id="page-content-wrapper">    
   <div class="container-fluid"> 
       <div class="row">
    		<div class="col-md-4">				
				<div class="row">
					<div class="progress"  style="margin-bottom:0px">
    					<div class="progress-bar progress-bar-striped active" id="myBar" role="progressbar"  aria-valuemax="100" style="width:0%"> </div>
  					</div>
				</div>
				<div class="row">
					<div class="reloj"></div>
					<br>
    				 <button type="button" id="stopwatch_bw" class="btn" onclick='buttonBackPress()' disabled>
     					  <span class="glyphicon glyphicon-backward"></span>
  				     </button>
    
  				     <button type="button" id="stopwatch_play" class="btn" onclick='buttonPlayPress();' disabled>
    					  <span class="glyphicon glyphicon-play"></span>
   					 </button>
    
   					 <button type="button" id="stopwatch_stop" class="btn" onclick='buttonStopPress();' disabled>
    					  <span class="glyphicon glyphicon-stop"></span>
    				 </button>
    
   					 <button type="button" id="stopwatch_fw" class="btn" onclick='buttonForwardPress()' disabled>
   						  <span class="glyphicon glyphicon-forward"></span>
   					 </button>
   					 <div id="speed"  style="display:inline-block; margin: 15px">
   					 	
   					 </div>
				</div>		
			</div>
			<div class="col-md-8"> 
				<table id="table">
					<thead><tr>
						<th data-field="Circle"></th>
						<th data-field="Start">Start</th>
						<th data-field="End">End</th>
						<th data-field="Period">Period</th>
						<th data-field="DeviceID">DeviceID</th>
						<th data-field="Track" ><center><span class="glyphicon glyphicon-map-marker"></span></center></th>
						<th data-field="Trash" ><center><span class="glyphicon glyphicon-trash"></span></center></th>
					</tr></thead>
				</table>
			</div>
	  </div>
       <div class="row">			
			<div class="col-lg-12">   
               <br />
                <div id='map'></div>
            </div>
       </div>
   </div>
</div>
        <!-- /#page-content-wrapper -->
</div>
    <!-- /#wrapper -->


<script>
<?php $urlRest="http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]."RestAPI";?>
var urlRest= <?php print '\''.$urlRest.'\''; ?>; //this variable contains the relative url of RestAPI folder

var mode=true; //true value: splitted mode; false value: single mode;

/*
 * this function implements the switching between single and splitted mode according to button '#track-mode' value
 */
$(function() {
	$('#track-mode').change(function() {
		mode=$(this).prop('checked');
		if(mode==false)
		{
    		$('#table').bootstrapTable('load',singleRow);
    		$('#table').bootstrapTable('refreshOptions',{detailFormatter:"detailFormatter",detailView:true});
			
			document.getElementById('percorsi').style.display = "none";
			if(singleRow.length==0)
			{
				stopwatch.setTime([0,0,0,0]);
				$("button[id^='stopwatch_']").prop('disabled',true);
			}
			else
			{	 
				stopwatch.setTime([singleStart.getHours(),singleStart.getMinutes(),singleStart.getSeconds(),singleStart.getMilliseconds()]);
				$("button[id^='stopwatch_']").prop('disabled',true);
				$("button[id='stopwatch_play']").prop('disabled',false);
			} 
			hideAllTrack(rows);
			showAllTrack(singleRow);
		}else
		{
			$('#table').bootstrapTable('load',rows);
			$('#table').bootstrapTable('refreshOptions',{detailView:false});

			if(checked.length==0)
			{
				stopwatch.setTime([0,0,0,0]);
				$("button[id^='stopwatch_']").prop('disabled',true);
			}
			else
			{	 
				stopwatch.setTime([arraystart[indexminstart].getHours(),arraystart[indexminstart].getMinutes(),arraystart[indexminstart].getSeconds(),arraystart[indexminstart].getMilliseconds()]);
				$("button[id^='stopwatch_']").prop('disabled',true);
				$("button[id='stopwatch_play']").prop('disabled',false);       			  		
			}
			hideAllTrack(singleRow);
			showAllTrack(rows);
		}
	})
});

//it creates the devices list
devicelist(urlRest);

//it creates the date range slider 
createSlider($("#slider"),urlRest);

/*
 * the side bar is toggled on play and stop buttons. 
 */
var toggled=false;

$("#stopwatch_play").click(function(e) {
  	if(!toggled){
	  	e.preventDefault();
	    $("#wrapper").toggleClass("toggled");
	    setTimeout(function(){window.dispatchEvent(new Event('resize'))}, 500);
	    toggled=true;
    }    
});

$("#stopwatch_stop").click(function(e) {
  	if(toggled){
	  	e.preventDefault();
	  	$("#wrapper").toggleClass("toggled");
	    toggled=false;
	    setTimeout(function(){$("#slider").dateRangeSlider("resize")}, 500);
	    
    }  
});
    
// istance of stopwatch object
var stopwatch = new Stopwatch(document.querySelector('.reloj'));

//text to display on caption table when no route is selected
$('#table').bootstrapTable({
    formatNoMatches: function () {
        return 'No route selected';
    }
});

//click event listener on device list (#myDevice element)
$('#myDevice').click(function(event){
	//it run a specific function according to mode value
	if(mode==true)
	{
		routelist(event);
	}
	else
	{
		singlemode(event);
	}
});



    
/*
 * Isance of map object. IMPORTANT: acces token must to be valid!!
 */
mapboxgl.accessToken = 'pk.eyJ1IjoibGVsbG96em85MSIsImEiOiJjaXlmcXZ0MTgwMDBrMnFtbWxrZXhsZjV5In0.ZuneiNmkXm55UcGa-fllKQ';
var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v9', //stylesheet location
    center: [13.513020, 43.617943], // starting position
    zoom: 18, // starting zoom
    trackResize: true,
    
});

/*
 * On load map event: POI are created, loaded is set on true value and devices list is auto-scrolled
 */
map.on('load', () => {
	createPOI(urlRest,map);
	setTimeout(function(){ 
		loaded=true;
		loading_screen.finish();
		$(".list-group").animate({scrollTop: $(document).height()}, 2000);        
		$(".list-group").animate({scrollTop: 0},900);
		}, 2000);		    
});


</script>

</body>

</html>
