<html>
	<head>
		<?php include 'scriptEventi.php';?>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Bootstrap Core CSS -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
    	<link href="css/custom.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
			<br />
			<div class="col-md-6 col-md-offset-3">
				<form enctype="multipart/form-data" method="post" role="form">
						<div class="panel panel-info">
							<div class="panel-heading"> <center> Caricamento dati da file CSV </center>
							</div>
							<center>
								<div class="panel-body">
									
									    <div class="form-group">
									        <label for="exampleInputFile" class="control-label">File Upload</label>
									        
									        <input type="file" name="file" id="file" size="250">
									        <p class="help-block">Only Excel/CSV File Import.</p>
									    </div>
									    
									    
									    <center><button type="submit" class="btn btn-info btn-celeste btn-md"  name="submit" value="Submit">Carica Dati</button></center><br/>
									   <!-- BOTTONI PER POPOLARE LE TABELLE DELLE CATEGORIE E DEGLI INDICATORI COMMENTATE -->
									  <!-- <center><button type="submit" class="btn btn-default"  name="Indicatori" value="Indicatori">Carica Indicatori</button>   <button type="submit" class="btn btn-default"  name="Categorie" value="Categorie">Carica Categorie</button></center>-->
				
								</div>
							</center>
						</div>
						
						
					</form>			
			</div>
		</div>
	</body>
</html>

