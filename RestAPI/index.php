<?php
// ** Framework API RESTful ** //

//applico il contest di applicazione json
header('Content-Type: application/json');

//includo tutti i file spiegati nelle precedenti parti
include_once ("conf.php");
include_once ("lib/db.php");
include_once ("lib/request.php");
include_once ("lib/echo.php");

// lancio la sessione
session_start();

//memorizzo il tipo di metodo richiesto e l'uri 
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);
define('REQUEST_URI', $_SERVER['REQUEST_URI']); //potrebbe http://localhost/frameworkapi/utente/13

//istanzio l'oggetto connessione al db
$DB = new Db();
$DB -> open();

/*spiego l'uri e identifico i vari parametri richiesti, in questo caso sono
 $args[0] = 'utente'
 $args[1] = 13
*/
$args=explane_uri(REQUEST_URI);
if (isset($args)) {
	$object = $args[0];
	$subject = null;
	$subject2 = null;
	$subject3=null;
	if (isset($args[1]))
		$subject = $args[1];
	if(isset($args[2]))
		$subject2 = $args[2];
	if(isset($args[3]))
		$subject3 = $args[3];
	

/*
In questa parte avviene lo smistamento delle operazioni.
Possiamo scrivere tutti i metodi che vogliamo, gestire anagrafiche,
creare procedure ad-hoc ,
l'importante è che si rispetti la regola dell' [oggetto]/[id_univoco] oppure
semplicemente [oggetto]
*/
    switch ($object) {
		case 'allStaticRoutenf':
			if($subject!=null)
				allStaticRounf($subject);
			break;
		case 'staticRoutenf':
			 if($subject!=null)
				staticRounf($subject,$subject2);
			break;
    	case 'sliderRoute':
			if($subject!=null && $subject2!=null && $subject3!=null)
				sliderRou($subject,$subject2,$subject3);
			break;
		case 'sliderDevice':
			if($subject!=null)
				sliderDev($subject,$subject2);
			break;
			
		case 'singleDevice':
			if($subject!=null)
				singleDev($subject,$subject2);
			break;
    	case 'permanenza':
			
			if($subject!=null)
				if($subject=="firstStart")
					firstStart();
					else if($subject=="lastEnd")
						lastEnd();
				else
            	permanenzaId($subject);
			else {
				permanenza();
			}
            break;
		
        case 'posizione':
			
			if($subject!=null)
            	posizioneId($subject,$subject2);
			else {
				posizione();
			}
            break;
        
        case 'posizionenf':
        	if($subject!=null)
        		posizionenfId($subject,$subject2);
        	else {
        		posizionenf();
        		}
        	break;
        
        case 'percorso':
        	if($subject!=null)
        		percorsoId($subject,$subject2);
        		else {
        			percorso();
        		}
        	break;
                    
		case 'device':
            if($subject!=null)
            	deviceId($subject);
			else {
				device();
			}
            break;
            
        case 'devicenf':
           	if($subject!=null)
            	deviceId($subject);
            else {
            	devicenf();
            }
            break;
            
		case 'beacon':
            if($subject!=null)
            	beaconId($subject);
			else {
				beacon();
			}
       		 break;
		default: 
			 generate404();//se nell'uri non trova nessuno dei metodi stabiliti, genera un errore
						  // 404 (matenendo il contesto json)
			break;
    }
} else{
	echo JechoAlert("Api online");
}
//************************************** LIST OF METHOD ********************************************//
 function staticRounf($id=null,$idPercorso=null){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
			$result = $DB->Db->query("SELECT * from posizionenf where DeviceID = '".$id ."' AND Percorso='".$idPercorso."' order by TimeIng");
			if($result->num_rows>0){
 				$jses = new Jecho();
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add([$row["Latitudine"],$row["Longitudine"]]);}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
			else {
				echo JechoErr('Error');
			}
		}
 	
 }
 
  function allStaticRounf($id=null){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
			$result = $DB->Db->query("SELECT * from posizionenf where DeviceID = '".$id ."' order by TimeIng");
			if($result->num_rows>0){
 				$jses = new Jecho();
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add([$row["Latitudine"],$row["Longitudine"]]);}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
			else {
				echo JechoErr('Error');
			}
		}
 	
 }
 
 
 function sliderRou($id=null,$min=null,$max=null){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
			$newMin = substr_replace($min, " ", 10, 0);
			$newMax = substr_replace($max, " ", 10, 0);
			$result = $DB->Db->query("SELECT * from percorso where DeviceID = '".$id ."' AND Start >= '".$newMin."' AND End <= '".$newMax ."' order by Start");
			if($result->num_rows>0){
 				$jses = new Jecho();
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add(((array)$row));}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
			else {
				echo JechoErr('Error');
			}
		}
	
 }
 function sliderDev($min=null,$max=null){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
			$newMin = substr_replace($min, " ", 10, 0);
			$newMax = substr_replace($max, " ", 10, 0);
			$result = $DB->Db->query("SELECT * from percorso where Start >= '".$newMin."' AND End <= '".$newMax ."' order by Start");
			if($result->num_rows>0){
 				$jses = new Jecho();
 
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add(((array)$row));}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
			else {
				echo JechoErr('Error');
			}
		}
	
 }
 
  function singleDev($min=null,$max=null){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
			$newMin = substr_replace($min, " ", 10, 0);
			$newMax = substr_replace($max, " ", 10, 0);
			$result = $DB->Db->query("SELECT * from permanenza where Start >= '".$newMin."' AND End <= '".$newMax ."' order by Start");
			if($result->num_rows>0){
 				$jses = new Jecho();
 
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add(((array)$row));}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
			else {
				echo JechoErr('Error');
			}
		}
	
 }
 
 
 function lastEnd(){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
		
			$result = $DB->Db->query("SELECT MAX(End) as Ultimo from permanenza");
			if($result->num_rows>0){
				$row = $result -> fetch_assoc();
                $jses = new Jecho((array)$row);
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Error');
			}
		}
	
 }
 
 function firstStart(){
 	global $DB;
	if (REQUEST_METHOD == "GET") {
		
			$result = $DB->Db->query("SELECT MIN(Start) as Primo from permanenza");
			if($result->num_rows>0){
				$row = $result -> fetch_assoc();
                $jses = new Jecho((array)$row);
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Error');
			}
		}
	
 }
 
 function permanenzaId($id=null){
 		
 	global $DB;
	if (REQUEST_METHOD == "GET") {
		if(isset($id)){
			$result = $DB->Db->query("SELECT * from permanenza WHERE DeviceID ='".$id."' order by Start");
			if($result->num_rows>0){
				$row = $result -> fetch_assoc();
                $jses = new Jecho((array)$row);
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Device non trovato');
			}
		}
	}else
		generate404();
 }
 
 function posizioneId($id=null,$idPercorso=null){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
		if(isset($id))
		{
			if(isset($idPercorso)){
				$result = $DB->Db->query("SELECT * from posizione WHERE DeviceID ='".$id."' and Percorso='".$idPercorso."' order by TimeIng");
			}else{
			
				$result = $DB->Db->query("SELECT * from posizione WHERE ID =".$id);
				if($result==false){
					$result = $DB->Db->query("SELECT * from posizione WHERE DeviceID ='".$id."' order by TimeIng");
				}
			}
			if($result->num_rows>0){
				$jses = new Jecho();
				
				while($row=mysqli_fetch_assoc($result)){
					$jses ->add(((array)$row));}
					$jses -> message = "Data loaded";
					echo $jses->encode();
				}
			else {
				echo JechoErr('Posizione non trovata');
			}
		}
	}else
		generate404();
 }

 function posizionenfId($id=null,$idPercorso=null){
 
 	global $DB;
 	if (REQUEST_METHOD == "GET") {
 		if(isset($id))
 		{
 			if(isset($idPercorso)){
 				$result = $DB->Db->query("SELECT * from posizionenf WHERE DeviceID ='".$id."' and Percorso='".$idPercorso."' order by TimeIng");
 			}else{
 					
 				$result = $DB->Db->query("SELECT * from posizionenf WHERE ID =".$id);
 				if($result==false){
 					$result = $DB->Db->query("SELECT * from posizionenf WHERE DeviceID ='".$id."' order by TimeIng");
 				}
 			}
 			if($result->num_rows>0){
 				$jses = new Jecho();
 
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add(((array)$row));}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
 			else {
 				echo JechoErr('Posizione non trovata');
 			}
 		}
 	}else
 		generate404();
 }

function permanenza(){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
			$result = $DB->Db->query("SELECT * from permanenza");
			if($result->num_rows>0){
				$jses = new Jecho();
				while($row=mysqli_fetch_assoc($result)){
					$jses->add((array)$row);
				}
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Posizione non trovata');
			}
	}else
		generate404();
 }
 
function posizione(){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
			$result = $DB->Db->query("SELECT * from posizione order by TimeIng");
			if($result->num_rows>0){
				$jses = new Jecho();
				while($row=mysqli_fetch_assoc($result)){
					$jses->add((array)$row);
				}
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Posizione non trovata');
			}
	}else
		generate404();
 }

 function posizionenf(){
 
 	global $DB;
 	if (REQUEST_METHOD == "GET") {
 		$result = $DB->Db->query("SELECT * from posizionenf order by TimeIng");
 		if($result->num_rows>0){
 			$jses = new Jecho();
 			while($row=mysqli_fetch_assoc($result)){
 				$jses->add((array)$row);
 			}
 			$jses -> message = "Data loaded";
 			echo $jses->encode();
 		}
 		else {
 			echo JechoErr('Posizione non trovata');
 		}
 	}else
 		generate404();
 }
 
 function percorso(){
 
 	global $DB;
 	if (REQUEST_METHOD == "GET") {
 		$result = $DB->Db->query("SELECT * from percorso order by Start");
 		if($result->num_rows>0){
 			$jses = new Jecho();
 			while($row=mysqli_fetch_assoc($result)){
 				$jses->add((array)$row);
 			}
 			$jses -> message = "Data loaded";
 			echo $jses->encode();
 		}
 		else {
 			echo JechoErr('Percorso non trovato');
 		}
 	}else
 		generate404();
 }
 
 function percorsoId($id=null,$idPercorso=null){
 
 	global $DB;
 	if (REQUEST_METHOD == "GET") {
 		if(isset($id))
 		{
 			if(isset($idPercorso)){
 				$result = $DB->Db->query("SELECT * from percorso WHERE DeviceID ='".$id."' and Route='".$idPercorso."' order by Start");
 			}else{
 
 				$result = $DB->Db->query("SELECT * from percorso WHERE DeviceID ='".$id."' order by Start");
 			}
 			if($result->num_rows>0){
 				$jses = new Jecho();
 
 				while($row=mysqli_fetch_assoc($result)){
 					$jses ->add(((array)$row));}
 					$jses -> message = "Data loaded";
 					echo $jses->encode();
 			}
 			else {
 				echo JechoErr('Percorso non trovato');
 			}
 		}
 	}else
 		generate404();
 }
 
function deviceId($id=null){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
		if(isset($id)){
			$result = $DB->Db->query("SELECT attributi.DeviceID,attributi.Chiave,attributi.Valore FROM attributi WHERE  attributi.DeviceID='".$id."'");
			if($result->num_rows>0){
				$jses = new Jecho();
				$data=array("DeviceID"=>$id);
				while($row=mysqli_fetch_assoc($result)){
					$data[$row['Chiave']]=$row['Valore'];}
				$jses ->add((array)$data);
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Posizione non trovata');
			}
		}
	}else
		generate404();
 }

function device(){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
		    $device = $DB->Db->query("SELECT ID from device");
		    if($device->num_rows>0){
		       $jses = new Jecho();
			   $data=array();
			   while ($id=mysqli_fetch_assoc($device)) {
			   		$result = $DB->Db->query("SELECT attributi.DeviceID,attributi.Chiave,attributi.Valore FROM attributi WHERE attributi.DeviceID='".$id['ID']."'");
					$data["DeviceID"]=$id['ID'];
					while($row=mysqli_fetch_assoc($result)){
						$data[$row['Chiave']]=$row['Valore'];
					}
					$jses ->add((array)$data);
					$data=array();
			   }
			   $jses -> message = "Data loaded";
			   echo $jses->encode();
			}
			else{
				echo JechoErr('Nessun device presente');
			}
	}else
		generate404();
 }
 
 function devicenf(){
 
 	global $DB;
 	if (REQUEST_METHOD == "GET") {
 		$device = $DB->Db->query("SELECT ID from devicenf");
 		if($device->num_rows>0){
 			$jses = new Jecho();
 			$data=array();
 			while ($id=mysqli_fetch_assoc($device)) {
 				$result = $DB->Db->query("SELECT attributi.DeviceID,attributi.Chiave,attributi.Valore FROM attributi WHERE attributi.DeviceID='".$id['ID']."'");
 				$data["DeviceID"]=$id['ID'];
 				while($row=mysqli_fetch_assoc($result)){
 					$data[$row['Chiave']]=$row['Valore'];
 				}
 				$jses ->add((array)$data);
 				$data=array();
 			}
 			$jses -> message = "Data loaded";
 			echo $jses->encode();
 		}
 		else{
 			echo JechoErr('Nessun device presente');
 		}
 	}else
 		generate404();
 }

function beaconId($id=null){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
		if(isset($id)){
			$result = $DB->Db->query("SELECT * from beacon WHERE ID =".$id);
			if($result->num_rows>0){
				$row = $result -> fetch_assoc();
                $jses = new Jecho((array)$row);
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Beacon non trovato');
			}
		}
	}else
		generate404();
 }

function beacon(){
 	
  	global $DB;
	if (REQUEST_METHOD == "GET") {
			$result = $DB->Db->query("SELECT * from beacon");
			if($result->num_rows>0){
				$jses = new Jecho();
				while($row=mysqli_fetch_assoc($result)){
					$jses->add((array)$row);
				}
				$jses -> message = "Data loaded";
				echo $jses->encode();
			}
			else {
				echo JechoErr('Beacons non trovati');
			}
	}else
		generate404();
 }
?>