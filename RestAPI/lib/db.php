<?php
class Db {
	public $Db;
	function __construct($open = null) {
		if (isset($open))
			$this ->Db-> open();
	}

	function open() {
		if (file_exists('conf.php'))
			include_once 'conf.php';
		elseif (file_exists('../conf.php'))
			include_once '../conf.php';

		if (file_exists('echo.php'))
			include_once 'echo.php';
		elseif (file_exists('../echo.php'))
			include_once '../echo.php';
		elseif (file_exists('lib/echo.php'))
			include_once 'lib/echo.php';
		
		// connessione al database
		$this -> Db = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

		// controlla se è in errore
		if (mysqli_connect_errno()) {
			echo JechoErr("Failed to connect to database: " . mysqli_connect_error());
			exit ;
		}

		//ritorna la connessione
		return ;
	}
}
?>