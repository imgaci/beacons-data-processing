<?php

function explane_uri($string) {
	$string =substr($string, strpos($string, END_POINT)+strlen(END_POINT));
	if ($string != "") {
		$vet = explode('/', $string);
		return $vet;
	} else
		return null;
}

function generate404() {
	include_once 'echo.php';

	header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
	header("Status: 404 Not Found");
	$_SERVER['REDIRECT_STATUS'] = 404;

	$jecho = new Jecho();
	$jecho -> status = false;
	$jecho -> server_status = 404;
	$jecho -> message = "error 404 page not found";
	echo $jecho -> encode();
	exit();
}
?>