<?php

class Jecho {

	private $data = array();
	public $status = true;
	public $server_status = 200;
	public $message = '';

	function __construct($var = null) {
		if (isset($var))
			$this -> data[] = $var;
	}

	public function add($var) {
		$this -> data[] = $var;
	}

	public function remove($var) {
		if (isset($this -> data[$var]))
			unset($this -> data[$var]);

	}

	public function encode() {
	
		$temp = array();
		$temp[] = array('status' => $this -> status, 'server_status' => $this -> server_status, 'message' => $this -> message, "data" => $this -> data);
		return json_encode($temp);
	}

}

/*
 * DESCRIZIONE : restituisce un flusso negativo con messaggio
 */
function JechoErr($message) {
	$jecho = new Jecho();
	$jecho -> status = false;
	$jecho -> server_status = 200;
	$jecho -> message = $message;
	return $jecho -> encode();
}

/*
 * DESCRIZIONE : restituisce un flusso positivo con messaggio
 */
function JechoAlert($message) {
	$jecho = new Jecho();
	$jecho -> message = $message;
	$jecho -> server_status = 200;
	return $jecho -> encode();
}
?>