# Database

## beacon.sql
This file contains a complete structure of used database and it is already populated.

### list of table:
- beacon contains beacons coordinates (NOT TO EMPTY )
- eventi contains all detected events with a field (isEntering) to indicate an entry or an exit 
- rilevamento contains the link between entries and exits
- posizionenf contains the intersections between the time intervals represented by Rilevamento elements. For each intersection is calculated the average of beacons coordinates like current position. (nf = no filter)
- devicenf contains the list of device (nf = no filter)
- posizione is the same of posizionenf but it is obtained with a filter on considered beacons (only 5 of 10)
- device is the same of devicenf
- attributi is a table for any device attributes with a syntax key-value  

### list of view:
- percorso list of route with start time, end time and difference between these
sql: CREATE VIEW percorso as SELECT DeviceID, Percorso as Route, MIN(TimeIng) as Start, MAX(TimeExit) as End, TIMEDIFF(MAX(TimeExit),MIN(TimeIng))as Period FROM posizionenf group BY DeviceID, Percorso

- permanenza: for each device, it contains the number of routes , the first entry,the last exit and the sum of all routes periods  
sql: CREATE VIEW permanenza AS SELECT DeviceID, count(Route) as N_Percorsi, MIN(Start) as Start, MAX(End) as End , SEC_TO_TIME(SUM(TIME_TO_SEC(Period)))as Period FROM percorso group BY DeviceID

## beaconspzzroma.csv
Is the source of data

# Application

## get started
First of all is needed to set database parameters like host, username and password in parametriDB.php and RestAPI/conf.php

## loader.php
This page can be used to load data from csv file on empty tables
 
## index.php
This page can be used to display on a map the routes stored on the database