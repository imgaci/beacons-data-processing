<?php

include 'parametriDB.php'; 

if(isset($_POST["submit"]))
{

$conn = mysql_connect($host,$db_user,$db_password) or die("Non riesco a connettermi al server MySql.");
mysql_select_db($db) or die (mysql_error());
   
 $file = $_FILES['file']['tmp_name'];
 $handle = fopen($file, "r");
 fgetcsv($handle);
 $c = 0;
 while(($emapData = fgetcsv($handle, 1000, ";")) !== false)
{	
	//insert of the element in the raw table
 	$sqlEventi = "INSERT into eventi (DeviceID, Beacon, IsEntering, Time) values ('$emapData[0]','$emapData[1]','$emapData[2]','$emapData[3]')";
 	$esito1=mysql_query($sqlEventi);

}
//a link between entrance and exit is made and stored in the table 'rilevamento'
$sqlDevice="SELECT distinct DeviceID from eventi";
$res_device=mysql_query($sqlDevice);
	while ($device=mysql_fetch_assoc($res_device)) {
		$ID=$device['DeviceID'];
		$maxtimeexit = array();
		$sqlIngressi = "SELECT * FROM eventi WHERE DeviceID='$ID' and IsEntering = '-1' order by Time" ;
		$res_ing=mysql_query($sqlIngressi);
		while($ingresso=mysql_fetch_assoc($res_ing))
		{
			$beacon=$ingresso['Beacon'];
			$timeing=$ingresso['Time'];
			if(!isset($maxtimeexit[$beacon]) || $timeing>$maxtimeexit[$beacon])
			{
				$sqlExit="SELECT * FROM eventi WHERE DeviceID='$ID' and Beacon ='$beacon' and Time >'$timeing' and timediff(Time,'$timeing')<'00:20:00.001' and IsEntering = '0'  order by Time" ;
				$res_exit=mysql_query($sqlExit);
				if($exit=mysql_fetch_assoc($res_exit))
				{
					$timeexit=$exit['Time'];
					$maxtimeexit[$beacon]=$timeexit;
					$sqlInsert="INSERT into rilevamento (DeviceID, Beacon,TimeIng,TimeExit) values ('$ID','$beacon','$timeing','$timeexit')";
					$res_insert=mysql_query($sqlInsert);
				}
			}
		}
		
	}
	$sqlBeacons="SELECT * from beacon";
	$res_beacons=mysql_query($sqlBeacons);
	//it creates an array that contains the beacon coordinates
	while ($key=mysql_fetch_assoc($res_beacons))
	{

		$beacons[$key["ID"]]["Longitudine"]=$key["Longitudine"];
		$beacons[$key["ID"]]["Latitudine"]=$key["Latitudine"];
	}
	
	//for each device, contemporaneity are analyzed and solved. Positions are calculated whit a filter on beacons set in the first case,
	//and with all of them in the second one.
	createPosizione($beacons,true);
	createPosizione($beacons,false);
	
	
 
 	if($esito1)
 	{
 		echo "You database has imported successfully";
	}else
	{
 		echo "Sorry! There is some problem.";
	}
}

function testContemporanea($i,$t,$analizzo)
	{
		$result=null;
		// Type 1 contemporaneity: two periods that have an intersection
		if($analizzo[$t]['TimeIng']<$analizzo[$i]['TimeExit'] && $analizzo[$t]['TimeExit']>$analizzo[$i]['TimeExit']){
					
					$risolta[0]=array("DeviceID"=>$analizzo[$i]['DeviceID'], "Beacon"=>$analizzo[$t]['Beacon'], "TimeIng"=>$analizzo[$i]['TimeExit'], "TimeExit"=>$analizzo[$t]['TimeExit']);
					$analizzo[$i]['TimeExit']=$analizzo[$t]['TimeIng'];
					$analizzo[$t]['TimeExit']=$risolta[0]['TimeIng'];
					$analizzo[$t]['Beacon']=$analizzo[$i]['Beacon']." ".$analizzo[$t]['Beacon'];
					
				    // sorted insert
				
					while($t+1<count($analizzo) && $risolta[0]['TimeIng']>$analizzo[$t+1]['TimeIng']){
						$t++;
					}
					array_splice($analizzo,$t+1,0,$risolta);
					$result=$analizzo;
				
				}
				// Type 2 contemporaneity: a period inside in another one
		else if($analizzo[$t]['TimeIng']<$analizzo[$i]['TimeExit'] && $analizzo[$i]['TimeExit']>$analizzo[$t]['TimeExit']){
		
					$risolta[0]= array("DeviceID"=>$analizzo[$i]['DeviceID'], "Beacon"=>$analizzo[$i]['Beacon'], "TimeIng"=>$analizzo[$t]['TimeExit'], "TimeExit"=>$analizzo[$i]['TimeExit']);	
					$analizzo[$i]['TimeExit']=$analizzo[$t]['TimeIng'];
					$analizzo[$t]['Beacon']=$analizzo[$i]['Beacon']." ".$analizzo[$t]['Beacon'];
				
					// sorted insert
					
					while($t+1<count($analizzo) && $risolta[0]['TimeIng']>$analizzo[$t+1]['TimeIng']){
						$t++;
					}
					
					array_splice($analizzo,$t+1,0,$risolta);
					$result=$analizzo;
		}
		return $result;
}

function mediaCoordinate($beacon,$coordinate)
{
	$list=explode(" ",$beacon);
	$sumlong=null;
	$sumlat=null;
	foreach ($list as $key)
	{
		$sumlong=$sumlong + $coordinate[$key]["Longitudine"];
		$sumlat=$sumlat + $coordinate[$key]["Latitudine"];
	}
	$result['Longitudine']=$sumlong/count($list);
	$result['Latitudine']=$sumlat/count($list);
	return $result;
}

function insertPosizione($rilevamento, $media, $percorso, $tabella)
{
	$devid=$rilevamento['DeviceID'];
	$beacon=$rilevamento['Beacon'];
	$timeing=$rilevamento['TimeIng'];
	$timeexit=$rilevamento['TimeExit'];
	$longitudine=$media['Longitudine'];
	$latitudine=$media['Latitudine'];
	$sqlInsert="INSERT into $tabella (DeviceID, Beacon,TimeIng,TimeExit,Longitudine,Latitudine,Percorso)
	values ('$devid','$beacon','$timeing','$timeexit','$longitudine','$latitudine','$percorso')";
	$res_insert=mysql_query($sqlInsert);
	return $res_insert;
}

function createPosizione($beacons,$boolfilter)
{
	//DeviceID are extracted without filter (minimum number of detections but no beacons restriction)
	//and the variables are setted for this case
	
	$sqlDevice="SELECT distinct DeviceID FROM rilevamento GROUP BY DeviceID HAVING count(ID)>2";
	$tabellaDev='devicenf';
	$tabellaPos='posizionenf';
	$string = "order by TimeIng" ;
	
	if($boolfilter==true)
	{
	
		//DeviceID are extracted with a filter (minimum number of detections and considered beacons restriction)
		//and the variables are setted for this case
		$sqlDevice="SELECT distinct DeviceID FROM rilevamento WHERE Beacon in (4,12,10,5,14) GROUP BY DeviceID HAVING count(ID)>2";
		$tabellaDev='device';
		$tabellaPos='posizione';
		$string = "AND Beacon in (4,12,10,5,14) order by TimeIng" ;
	}
	
	$res_device=mysql_query($sqlDevice);
	while ($grezzi=mysql_fetch_assoc($res_device)) {
		$ID=$grezzi['DeviceID'];
		$res_insert=mysql_query("insert into $tabellaDev (ID) values ('$ID')");
		$analizzo = array();
		$da_analizzare = array();
		$res_estratti=mysql_query("SELECT * FROM rilevamento WHERE DeviceID='$ID' ".$string);
		$NumPercorso=0;
		$LastExit=new DateTime('2000-01-01 00:00:00.000');
		while($estratto=mysql_fetch_assoc($res_estratti)){
			array_push($analizzo,$estratto);
		}
	
		$i=0;

		//each event is analyzed with the following ones (as long as necessary), using two indexes ($ i and $ t), to see if it generates contemporaneity, 
		//and solving them always keeping sorted on entry times the records vector.
		while ($i<count($analizzo)) {
			$t=$i+1;
				
			while($t<count($analizzo)){
					
				$array=testContemporanea($i, $t, $analizzo);
				
				//if $array is null, it means that no one contemporaneity has been found and so this detection can be insert
				//in the table 'posizione', then the indexes are incremented
				if($array==null){
					if($analizzo[$i]['TimeIng']<$analizzo[$i]['TimeExit'])
					{
						$media=mediaCoordinate($analizzo[$i]['Beacon'],$beacons);

					    //It checks if the difference between this entry and the last exit is greater than 5 minutes
						//and, in this case, the event will be associated with a new route by incrementing the appropriate index
						$a=new DateTime($analizzo[$i]['TimeIng']);
						$diff = abs(strtotime($LastExit->format('d-m-Y H:i:s.u'))-strtotime($a->format('d-m-Y H:i:s.u')));
						if($diff>300)
						{
							$NumPercorso++;
						}
						$LastExit=new DateTime($analizzo[$i]['TimeExit']);
						//Insert in the table 'posizione'
						$res_insert=insertPosizione($analizzo[$i],$media,$NumPercorso,$tabellaPos);
					}
					$i++;
					$t=$i+1;
				}

				//A contemporaneity was detected thus the array to be analyzed is updated ($analizzo)
				//and then the tests continue
				else
					$analizzo=$array;
			}
				
				
			$media=mediaCoordinate($analizzo[$i]['Beacon'],$beacons);
			//It checks if the difference between this entry and the last exit is greater than 5 minutes
			//and, in this case, the event will be associated with a new route by incrementing the appropriate index
			$a=new DateTime($analizzo[$i]['TimeIng']);
			$diff = abs(strtotime($LastExit->format('d-m-Y H:i:s.u'))-strtotime($a->format('d-m-Y H:i:s.u')));
			if($diff>300)
			{
				$NumPercorso++;
			}
			$LastExit=new DateTime($analizzo[$i]['TimeExit']);
			
			//insert of the last detections which has not following events to generate contemporaneity
			$res_insert=insertPosizione($analizzo[$i],$media,$NumPercorso,$tabellaPos);
				
			$i++;
				
				
		}
	
	}
return;
}

?>										